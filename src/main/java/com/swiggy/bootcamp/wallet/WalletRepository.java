package com.swiggy.bootcamp.wallet;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
interface WalletRepository extends CrudRepository<Wallet, Long> {

    List<Wallet> findAllByUserId(int userId);
}
