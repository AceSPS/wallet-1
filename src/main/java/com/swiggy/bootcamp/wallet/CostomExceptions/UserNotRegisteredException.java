package com.swiggy.bootcamp.wallet.CostomExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "User not Registered")
public class UserNotRegisteredException extends RuntimeException {
}
