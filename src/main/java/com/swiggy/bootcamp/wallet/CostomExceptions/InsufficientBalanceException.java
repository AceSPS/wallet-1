package com.swiggy.bootcamp.wallet.CostomExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Insufficient balance")

public class InsufficientBalanceException extends RuntimeException {
}