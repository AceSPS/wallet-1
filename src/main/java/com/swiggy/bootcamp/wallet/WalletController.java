package com.swiggy.bootcamp.wallet;

import com.swiggy.bootcamp.wallet.CostomExceptions.InsufficientBalanceException;
import com.swiggy.bootcamp.wallet.CostomExceptions.UserNotRegisteredException;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/wallets")
@RestController
class WalletController {
    @Autowired
    private WalletService walletService;

    WalletController(WalletService walletService) {
        this.walletService = walletService;
    }

    @GetMapping
    Wallet getBalance(@RequestParam("userId") int userId) throws UserNotRegisteredException {
        return walletService.getWalletForUser(userId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Wallet createWallet(@Valid @RequestBody Wallet wallet) {
        return walletService.createWallet(wallet);
    }

    @GetMapping("{id}/transactions")
    List<Transaction> getTransactions(@PathVariable int id){
        return walletService.getTransactions(id);
    }

    @PostMapping("{id}/transactions")
    Wallet updateBalance(@PathVariable int id,@RequestBody Transaction transaction) throws UserNotRegisteredException, InsufficientBalanceException {
        return walletService.transact(id, transaction);
    }
}
