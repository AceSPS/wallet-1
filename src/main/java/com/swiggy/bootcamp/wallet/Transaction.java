package com.swiggy.bootcamp.wallet;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Getter
@Entity
@NoArgsConstructor
@Table(name = "wallet_transaction")
@EqualsAndHashCode(exclude = {"id", "wallet"})

class Transaction {
    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int money;
    private Date date;
    @JsonIgnore
    @JoinColumn
    @ManyToOne(targetEntity = Wallet.class)
    Wallet wallet;


    @JsonProperty
    @Enumerated(value = EnumType.ORDINAL)
    TransactionType type;

    Transaction(int money, TransactionType type) {
        this.type = type;
        this.money = money;
    }

    public void linkWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public int amount() {
        return type == TransactionType.CREDIT ? money : -1 * money;
    }

    enum TransactionType {
        CREDIT, DEBIT;
    }
}
