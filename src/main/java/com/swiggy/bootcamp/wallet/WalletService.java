package com.swiggy.bootcamp.wallet;

import com.swiggy.bootcamp.wallet.CostomExceptions.InsufficientBalanceException;
import com.swiggy.bootcamp.wallet.CostomExceptions.UserNotRegisteredException;
import org.graalvm.compiler.replacements.amd64.AMD64StringIndexOfNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// Represents a collection of wallets
@Service
class WalletService {
    @Autowired
    WalletRepository walletRepository;

    WalletService(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }

    Wallet createWallet(int userId, int balance) {
        Wallet wallet = new Wallet(userId, balance);
        this.walletRepository.save(wallet);
        return wallet;
    }

    Wallet createWallet(Wallet wallet) {
        this.walletRepository.save(wallet);
        return wallet;
    }

    Wallet getWalletForUser(int userId) {
        List<Wallet> wallets = this.walletRepository.findAllByUserId(userId);
        if (wallets == null || wallets.size() == 0) {
            throw new UserNotRegisteredException();
        }
        return wallets.get(0);
    }


    public Wallet transact(int userId, Transaction transaction) {
        Wallet wallet = getWalletForUser(userId);
        wallet.processTransaction(transaction);
        walletRepository.save(wallet);
        return wallet;
    }

    public List<Transaction> getTransactions(int id) {
        Wallet wallet = getWalletForUser(id);
        return wallet.transactionList;
    }
}
