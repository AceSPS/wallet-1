package com.swiggy.bootcamp.wallet;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.bootcamp.wallet.CostomExceptions.InsufficientBalanceException;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"transactions", "id" })
@Entity
class Wallet {
    @JsonIgnore
    @Id
    @GeneratedValue
    Long id;

    @JsonProperty
    private int userId;

    @JsonProperty
    @Range(min = 10, max = 100000)
    private int balance;

    @JsonIgnore
    @OneToMany(mappedBy = "wallet", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    List<Transaction> transactionList = new ArrayList<>(0);

    Wallet(int userId, int balance) {
        this.userId = userId;
        this.balance = balance;
    }

    public void processTransaction(Transaction transaction) {
        int newBalance = this.balance + transaction.amount();
        if (newBalance < 0)
            throw new InsufficientBalanceException();
        this.balance = newBalance;
        transactionList.add(transaction);
        transaction.linkWallet(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Wallet wallet = (Wallet) o;
        return userId == wallet.userId &&
                balance == wallet.balance;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, balance);
    }
}
