package com.swiggy.bootcamp.wallet;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
class GreetingController {
    @GetMapping("/greeting")
    String greeting(@RequestParam("name") String name){
        return "Hello, "+ name + "!";
    }
}
