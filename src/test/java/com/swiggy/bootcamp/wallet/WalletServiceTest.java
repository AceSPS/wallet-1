package com.swiggy.bootcamp.wallet;

import com.swiggy.bootcamp.wallet.CostomExceptions.InsufficientBalanceException;
import com.swiggy.bootcamp.wallet.CostomExceptions.UserNotRegisteredException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class WalletServiceTest {
    private WalletService walletService;
    @Autowired
    private WalletRepository walletRepository;

    @BeforeEach
    void setup() {
        walletService = new WalletService(walletRepository);
        walletRepository.save(new Wallet(3, 1000));
        walletRepository.save(new Wallet(4, 1000));
        walletRepository.save(new Wallet(5, 1000));
        walletRepository.save(new Wallet(7, 1000));
    }

    @Nested
    class TestGetWalletForUser {
        @Test
        void shouldReturnWalletForUser() throws UserNotRegisteredException {
            assertEquals(new Wallet(3, 1000), walletService.getWalletForUser(3));
        }

        @Test
        void shouldThrowUserNotRegisteredExceptionWhenUserNotRegistered() {
            assertThrows(UserNotRegisteredException.class, () -> {
                walletService.getTransactions(1000);
            });
        }
    }

    @Test
    void shouldAddWalletForUser() {
        assertEquals(new Wallet(6, 1000), walletService.createWallet(6, 1000));
    }

    @Nested
    class TestTransact {
        @Test
        void shouldCreditToWallet() {
            assertEquals(new Wallet(7, 2000), walletService.transact(7, new Transaction(1000, Transaction.TransactionType.CREDIT)));
        }

        @Test
        void shouldDebitFromWallet() {
            assertEquals(new Wallet(4, 500), walletService.transact(4, new Transaction(500, Transaction.TransactionType.DEBIT)));
        }

        @Test
        void shouldThrowUserNotRegisteredExceptionWhenUserNotRegistered() {
            assertThrows(UserNotRegisteredException.class, () -> {
                walletService.transact(1000, new Transaction(500, Transaction.TransactionType.DEBIT));
            });
        }

        @Test
        void shouldThrowInsufficientBalanceExceptionWhenUserNotRegistered() {
            assertThrows(InsufficientBalanceException.class, () -> {
                walletService.transact(4, new Transaction(1100, Transaction.TransactionType.DEBIT));
            });
        }
    }

    @Test
    void shouldReturnTransactions() {
        Transaction transaction = new Transaction(100, Transaction.TransactionType.CREDIT);
        Transaction anotherTransaction = new Transaction(100, Transaction.TransactionType.DEBIT);
        ArrayList<Transaction> transactions = new ArrayList<>(Arrays.asList(transaction,anotherTransaction));
        walletService.transact(5,transaction);
        walletService.transact(5,anotherTransaction);

        assertEquals(transactions,walletService.getTransactions(5));
    }

}
