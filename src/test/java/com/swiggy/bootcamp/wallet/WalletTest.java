package com.swiggy.bootcamp.wallet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class WalletTest {
    private String convertPOJOToJSONString(Wallet wallet) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(wallet);
    }

    private Wallet convertJSONStringToPOJO(String input) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(input, Wallet.class);
    }

    @Test
    void walletToJson() throws JsonProcessingException {
        Wallet wallet = new Wallet(2,3);
        assertEquals("{\"userId\":2,\"balance\":3}", convertPOJOToJSONString(wallet));
    }
    @Test
    void jsonToWallet() throws IOException {
        Wallet wallet = new Wallet(2,3);
        assertEquals(new Wallet(2,3), convertJSONStringToPOJO("{\"userId\":2,\"balance\":3}"));
    }


}