package com.swiggy.bootcamp.wallet;

import com.swiggy.bootcamp.wallet.CostomExceptions.InsufficientBalanceException;
import com.swiggy.bootcamp.wallet.CostomExceptions.UserNotRegisteredException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WalletController.class)
class WalletControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private WalletService walletService;


    @Nested
    class GetWallet {
        @Test
        void testGetWalletSuccessfully() throws Exception {
            Mockito.when(walletService.getWalletForUser(1)).thenReturn(new Wallet(1, 500));

            mockMvc.perform(get("/wallets?userId=1"))
                    .andExpect(status().isOk())
                    .andExpect(content().json("{\"userId\":1, \"balance\":500}"));
        }

        @Test
        void shouldThrowUserNotRegisteredExceptionWhenUserNotRegistered() throws Exception {
            Mockito.when(walletService.getWalletForUser(2)).thenThrow(UserNotRegisteredException.class);

            mockMvc.perform(get("/wallets?userId=2"))
                    .andExpect(status().is4xxClientError());
        }
    }

    @Nested
    class CreateWallet {
        @Test
        void testCreateWallet() throws Exception {
            Mockito.when(walletService.createWallet(new Wallet(10, 500))).thenReturn(new Wallet(10, 500));

            mockMvc.perform(post("/wallets").content("{\"userId\":10, \"balance\":500}")
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isCreated())
                    .andExpect(content().json("{\"userId\":10, \"balance\":500}"));
        }
    }

    @Nested
    class TransactTest {
        @Test
        void testAddBalance() throws Exception {
            Mockito.when(walletService.transact(200, new Transaction(500, Transaction.TransactionType.CREDIT)))
                    .thenReturn(new Wallet(200, 1000));

            mockMvc.perform(post("/wallets/200/transactions").content("{\"money\":500, \"type\":\"CREDIT\"}")
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content().json("{\"userId\":200, \"balance\":1000}"));
        }
//
//        @Test
//        void shouldThrowUserNotRegisteredExceptionWhenUserNotRegistered() throws Exception {
//            Mockito.when(walletService.addBalance(5, 500)).thenThrow(UserNotRegisteredException.class);
//
//            mockMvc.perform(put("/wallets?userId=2").content("{\"userId\":10, \"money\":500}")
//                    .contentType(MediaType.APPLICATION_JSON))
//                    .andExpect(status().is4xxClientError());
//        }
//
//        @Test
//        void shouldSubtractSuccessfully() throws Exception, InsufficientBalanceException {
//            Mockito.when(walletService.subtractBalance(5, 500)).thenReturn(new Wallet(5, 1000));
//
//            mockMvc.perform(put("/wallets/debit").content("{\"userId\":5, \"money\":500}")
//                    .contentType(MediaType.APPLICATION_JSON))
//                    .andExpect(status().isOk())
//                    .andExpect(content().json("{\"userId\":5, \"balance\":1000}"));
//        }
//
//        @Test
//        void shouldThrowUserNotRegisteredExceptionWhenUserNotRegistered() throws Exception, InsufficientBalanceException {
//            Mockito.when(walletService.subtractBalance(6, 500)).thenThrow(UserNotRegisteredException.class);
//
//            mockMvc.perform(put("/wallets/debit").content("{\"userId\":6, \"money\":500}")
//                    .contentType(MediaType.APPLICATION_JSON))
//                    .andExpect(status().isBadRequest());
//        }
//
//        @Test
//        void shouldThrowInsufficientBalanceExceptionWhenBalanceIsLessThanDebitAmount() throws Exception, InsufficientBalanceException {
//            Mockito.when(walletService.subtractBalance(8, 500)).thenThrow(InsufficientBalanceException.class);
//
//            mockMvc.perform(put("/wallets/debit").content("{\"userId\":8, \"money\":500}")
//                    .contentType(MediaType.APPLICATION_JSON))
//                    .andExpect(status().isBadRequest());
//        }
    }
}